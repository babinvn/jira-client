const https = require('https');
const axios = require('axios');
const qs = require('qs');
const agent = new https.Agent({ rejectUnauthorized: false });

async function get(options, url, params) {
  options = options || {};
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await axios.get(`${options.baseaddr}${url}`,
      {
        params,
        auth: {
          username: options.username,
          password: options.password
        },
        httpsAgent: agent,
        headers: { 'x-atlassian-token': 'no-check', ...options.headers },
        paramsSerializer: params => qs.stringify(params, { arrayFormat: 'repeat' })
      });
      resolve(resp.data);
    } catch(err) {
      reject(err);
    }
  })
}
async function put(options, url, data){
  options = options || {};
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await axios.put(`${options.baseaddr}${url}`,
        data,
        {
          auth: {
            username: options.username,
            password: options.password
          },
          httpsAgent: agent,
          headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'x-atlassian-token': 'no-check',
            ...options.headers
          }
        }
      );
      resolve(resp.data);
    } catch(err) {
      reject(err);
    }
  })
}
async function putXml(options, url, data){
  options = options || {};
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await axios.put(`${options.baseaddr}${url}`,
        data,
        {
          auth: {
            username: options.username,
            password: options.password
          },
          httpsAgent: agent,
          headers: {
            'Content-Type': 'text/xml;charset=utf-8',
            'x-atlassian-token': 'no-check',
            ...options.headers
          }
        }
      );
      resolve(resp.data);
    } catch(err) {
      reject(err);
    }
  })
}
async function post(options, url, data){
  options = options || {};
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await axios.post(`${options.baseaddr}${url}`,
        data,
        {
          auth: {
            username: options.username,
            password: options.password
          },
          httpsAgent: agent,
          headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'x-atlassian-token': 'no-check',
            ...options.headers
          }
        }
      );
      resolve(resp.data);
    } catch(err) {
      reject(err);
    }
  })
}
async function postForm(options, url, data){
  options = options || {};
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await axios.post(`${options.baseaddr}${url}`,
        qs.stringify(data),
        {
          auth: {
            username: options.username,
            password: options.password
          },
          httpsAgent: agent,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-atlassian-token': 'no-check',
            ...options.headers
          }
        }
      );
      resolve(resp.data);
    } catch(err) {
      reject(err);
    }
  })
}
async function del(options, url, params) {
  options = options || {};
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await axios.delete(`${options.baseaddr}${url}`,
      {
        params,
        auth: {
          username: options.username,
          password: options.password
        },
        httpsAgent: agent,
        headers: { 'x-atlassian-token': 'no-check', ...options.headers }
      });
      resolve(resp.data);
    } catch(err) {
      reject(err);
    }
  })
}

function Client(options) {
  this.options = options;
}

Client.prototype.get = async function (url, params, options) {
  return get({...this.options, ...options}, url, params);
}
Client.prototype.post = async function (url, data, options) {
  return post({...this.options, ...options}, url, data);
}
Client.prototype.postForm = async function (url, data, options) {
  return postForm({...this.options, ...options}, url, data);
}
Client.prototype.put = async function (url, data, options) {
  return put({...this.options, ...options}, url, data);
}
Client.prototype.putXml = async function (url, data, options) {
  return putXml({...this.options, ...options}, url, data);
}
Client.prototype.del = async function (url, params, options) {
  return del({...this.options, ...options}, url, params);
}

module.exports = Client;
