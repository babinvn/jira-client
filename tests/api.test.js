const fs = require('fs');
const { join } = require('path');
const JiraClient = require('../index');
const options = require('../.test.settings.js');
const client = new JiraClient(options);

const {
  testListProjectCategories, testGetProjectCategory, testListProjectForCategory
} = require('./js/projects');
// const { delay } = require('./js/util');
const { testGetIssue } = require('./js/issue');
const { testCreateWorklog } = require('./js/worklog');

describe('Jira API tests', () => {
  beforeAll(async () =>
    new Promise(async (resolve, reject) => {
      try {
        // TODO
        resolve();
      } catch(err) {
        reject(err);
      }
    })
  , 60000);

  beforeEach(async () => {
    jest.setTimeout(1000000);
  });

  it.skip('Test get issue', async () => testGetIssue(client, 'ERP-337'));
  it.skip('Test list project categories', async () => testListProjectCategories(client));
  it.skip('Test get project category', async () => testGetProjectCategory(client, 'Agreements'));
  it.skip('Test list projects for category', async () => testListProjectForCategory(client, 'Agreements'));

  it('Test create worklog', async () => testCreateWorklog(client));

});
