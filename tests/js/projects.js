async function testListProjectCategories(client) {
  const projectCategories = await client.listProjectCategories();
  console.debug(projectCategories);
}
async function testGetProjectCategory(client, name) {
  let projectCategory = await client.findProjectCategory(name);
  projectCategory = await client.getProjectCategory(projectCategory.id);
  console.debug(projectCategory);
}
async function testListProjectForCategory(client, categoryName) {
  let projects = await client.listProjects({ expand: 'projectKeys' });
  projects = projects.filter(project => project.projectCategory.name === categoryName);
  expect(projects.length > 0).toBe(true);
  console.debug(projects.length);
}

module.exports = {
  testListProjectCategories, testGetProjectCategory, testListProjectForCategory
}
