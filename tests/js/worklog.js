async function testCreateWorklog(jiraClient) {
  await jiraClient.createWorklog(2038846, 'admin', {
    started: `2020-03-01T10:00:00.000-0000`,
    timeSpentSeconds: 600,
  });
}

module.exports = { testCreateWorklog }
