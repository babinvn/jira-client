const delay = async ms => new Promise(resolve => setTimeout(() => resolve(), ms));

module.exports = { delay };
