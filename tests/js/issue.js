async function testGetIssue(client, issueKey) {
  const issue = await client.getIssue(issueKey);
  console.debug(issue);
}

module.exports = {
  testGetIssue
}
