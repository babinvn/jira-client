// https://docs.atlassian.com/software/jira/docs/api/REST/8.9.1/#api/2/issue-editIssue

const BaseClient = require('./base');

function JiraClient(options) {
    BaseClient.apply(this, [options]);
}
JiraClient.prototype = Object.create(BaseClient.prototype);
JiraClient.prototype.constructor = JiraClient;

JiraClient.prototype.search = async function (params) {
  return this.get('/rest/api/2/search', params);
}
JiraClient.prototype.searchAll = async function (params) {
  const issues = [];
  params.maxResults = params.maxResults || 100;
  params.startAt = params.startAt || 0;

  while (true) {
    const data = await this.get('/rest/api/2/search', params);
    data.issues.forEach(issue => issues.push(issue));
    if (data.issues.length < params.maxResults)
      break;
    params.startAt += params.maxResults;
  }
  return issues;
}

JiraClient.prototype.listProjectCategories = async function() {
  return this.get('/rest/api/2/projectCategory');
}
JiraClient.prototype.findProjectCategory = async function(name) {
  const projectCategories = (await this.get('/rest/api/2/projectCategory'))
    .filter(projectCategory => projectCategory.name === name);
  return projectCategories.length > 0 ? projectCategories[0] : null;
}
JiraClient.prototype.getProjectCategory = async function(id, params) {
  return await this.get(`/rest/api/2/projectCategory/${id}`, params);
}

JiraClient.prototype.getPermissionScheme = async function (id) {
  return this.get(`/rest/api/2/permissionscheme/${id}`);
}
JiraClient.prototype.updatePermissionScheme = async function (id, permissions) {
  return this.put(`/rest/api/2/permissionscheme/${id}`, { permissions });
}

JiraClient.prototype.listProjects = async function(params) {
  return this.get('/rest/api/2/project', params);
}
JiraClient.prototype.getProject = async function (projectKey) {
  return this.get(`/rest/api/2/project/${projectKey}`);
}
JiraClient.prototype.deleteProject = async function (projectKey) {
  return this.del(`/rest/api/2/project/${projectKey}`);
}

JiraClient.prototype.getIssue = async function(key, params) {
  return this.get(`/rest/api/2/issue/${key}`, params);
}
JiraClient.prototype.putIssue = async function(key, params) {
  return this.put(`/rest/api/2/issue/${key}`, params);
}
JiraClient.prototype.deleteIssue = async function (issueId) {
  return this.del(`/rest/api/2/issue/${issueId}?deleteSubtasks=true`);
}

JiraClient.prototype.deleteWorklog = async function (worklog) {
  return this.del(`/rest/api/2/issue/${worklog.issueid}/worklog/${worklog.id}`);
}
JiraClient.prototype.createWorklog = async function (issueId, author, worklog) {
  return this.post(`/rest/api/2/issue/${issueId}/worklog`, worklog, {
    headers: { sudo: author }
  });
}

JiraClient.prototype.setUserPassword = async function (username, password) {
  return this.put(`/rest/api/2/user/password?username=${username}`, { password });
}
JiraClient.prototype.setUserActive = async function (username, active) {
  return this.put(`/rest/api/2/user?username=${username}`, { active });
}
JiraClient.prototype.deleteUser = async function (username) {
  return this.del(`/rest/api/2/user/${username}`);
}

module.exports = JiraClient;
