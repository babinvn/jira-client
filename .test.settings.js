module.exports = {
  baseaddr: process.env.LOCAL_JIRA_HOME,
  username: process.env.LOCAL_JIRA_USER,
  password: process.env.LOCAL_JIRA_PASSWORD,
}
